# create cluster with substituted values.
eksctl create cluster -f eks/cluster.yaml
  
#eksctl create cluster \
#		--version 1.27 \
#		--name $ENVIRONMENT \
#		--region $REGION \
#		--fargate
#		-dry-run  

# create ALB controller
aws iam create-policy \
    --policy-name AWSLoadBalancerControllerIAMPolicy \
    --policy-document file://eks/alb-iam-policy.json
    

 eksctl create iamserviceaccount \
   --cluster=$ENVIRONMENT \
   --region $REGION \
   --namespace=kube-system \
   --name=aws-load-balancer-controller \
   --attach-policy-arn=arn:aws:iam::$ACC_ID:policy/AWSLoadBalancerControllerIAMPolicy \
   --approve
  
 helm repo add eks https://aws.github.io/eks-charts
 helm install aws-load-balancer-controller eks/aws-load-balancer-controller \
   -n kube-system \
   --set clusterName=$ENVIRONMENT \
   --set serviceAccount.create=false \
   --set serviceAccount.name=aws-load-balancer-controller

# create CSI ESB driver which is required for PVC which are used by Bitnami PostgreSQL chart
# for an encrypted EBS please see: https://docs.aws.amazon.com/eks/latest/userguide/csi-iam-role.html
# eksctl create iamserviceaccount \
#   --cluster $ENVIRONMENT \
#   --region $REGION \
#   --name ebs-csi-controller-sa \
#   --namespace kube-system \
#   --attach-policy-arn arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy \
#   --approve \
#   --role-only \
#   --role-name AmazonEKS_EBS_CSI_DriverRole
  
# eksctl create addon \
#   --cluster $ENVIRONMENT \
#   --region $REGION \
#   --name aws-ebs-csi-driver \
#   --service-account-role-arn arn:aws:iam::$ACC_ID:role/AmazonEKS_EBS_CSI_DriverRole
      
# ALB_IAM_POLICY = $(aws iam list-policies --query "Policies[?PolicyName==`AWSLoadBalancerControllerIAMPolicy`].Arn" --output text)

