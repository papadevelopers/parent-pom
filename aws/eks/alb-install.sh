#aws iam create-policy \
#    --policy-name AWSLoadBalancerControllerIAMPolicy \
#    --policy-document file://eks/alb-iam-policy.json
    

# eksctl create iamserviceaccount \
#   --cluster=$ENVIRONMENT \
#   --region $REGION \
#   --namespace=kube-system \
#   --name=aws-load-balancer-controller \
#   --attach-policy-arn=arn:aws:iam::$ACC_ID:policy/AmazonEKS_AWSLoadBalancerControllerIAMPolicy \
#   --approve
  
# helm repo add eks https://aws.github.io/eks-charts
# helm install aws-load-balancer-controller eks/aws-load-balancer-controller \
#   -n kube-system \
#   --set clusterName=$ENVIRONMENT \
#   --set serviceAccount.create=false \
#   --set serviceAccount.name=aws-load-balancer-controller
   
   
helm upgrade -i aws-load-balancer-controller eks/aws-load-balancer-controller \
    --set clusterName=$ENVIRONMENT \
    --set serviceAccount.create=true \
    --set region=$REGION \
    --set vpcId=$. \
    --set serviceAccount.name=aws-load-balancer-controller \
    -n kube-system
    
   
