# deploy EFS storage driver
#kubectl apply -f test/efs-driver.yaml
# Get CIDR range
CIDR_RANGE=$(aws ec2 describe-vpcs --vpc-ids "$VPC_ID" --query "Vpcs[].CidrBlock" --output text)

# security for our instances to access file storage
# Create the security group and capture the output as JSON
SG_GROUP_ID=$(aws ec2 create-security-group --group-name "$ENVIRONMENT"-eks-efs-sg --description eks-efs-security-group --vpc-id $VPC_ID --output text --query "GroupId")
echo "Security group created for efs : $SG_GROUP_ID"
aws ec2 authorize-security-group-ingress --group-id "$SG_GROUP_ID"  --protocol tcp --port 2049 --cidr $CIDR_RANGE

# create storage
EFS_ID=$(aws efs create-file-system --creation-token eks-efs --output text --query "FileSystemId")

# create mount point 
# Check if the resource was created successfully, and retry if not
num_retries=0
while [ -z "$LIFECYCLE_STATE" ] && [ $num_retries -lt $MAX_RETRIES ]; do
  echo "Retry count $num_retries"
  sleep $RETRY_INTERVAL
  LIFECYCLE_STATE=$(aws efs describe-file-systems --file-system-id $EFS_ID --query 'FileSystems[0].LifeCycleState' --output text)
  num_retries=$((num_retries+1))
done

if [ -n "$LIFECYCLE_STATE" ]; then
	aws efs create-mount-target --file-system-id $EFS_ID --subnet-id "${PRIVATE_SUBNETS[0]}" --security-group $SG_GROUP_ID	
	aws efs create-mount-target --file-system-id $EFS_ID --subnet-id "${PRIVATE_SUBNETS[1]}" --security-group $SG_GROUP_ID	
else
  echo "failed to get available state of efs volume after $MAX_RETRIES retries"
  exit 1
fi

# grab our volume handle to update our PV YAML
aws efs describe-file-systems --query "FileSystems[*].FileSystemId" --output text