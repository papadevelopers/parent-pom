# required properties
EFS_ID=$(aws efs describe-file-systems --query "FileSystems[*].FileSystemId" --output text)

DIRECTORY="./jenkins/k8s"
for file in $DIRECTORY/*.yaml; do
	( . ./configs/env.sh; envsubst ) < $file > $file.tmp
done

#eksctl create fargateprofile --namespace jenkins --cluster $ENVIRONMENT --region $REGION
kubectl create namespace jenkins

#Create load balancer for public ip accessiblity
echo "Creating load balancer for jenkins"
kubectl apply -f jenkins/k8s/jenkins-alb-rbac.yaml.tmp

#serviceaccounts that exist in Kubernetes will be excluded, use --override-existing-serviceaccounts to override
eksctl create iamserviceaccount \
--name jenkins-alb-ingress-controller \
--namespace jenkins \
--cluster $ENVIRONMENT \
--attach-policy-arn arn:aws:iam::$ACC_ID:policy/AWSLoadBalancerControllerIAMPolicy \
--approve		

kubectl apply -f jenkins/k8s/jenkins-alb-deployment.yaml.tmp
kubectl get pods -n jenkins
echo "load balancer pod created for jenkisn"

kubectl get storageclass
#create volume
kubectl apply -f jenkins/k8s/jenkins-pv.yaml.tmp 
kubectl get pv

# create volume claim
kubectl apply -f jenkins/k8s/jenkins-pvc.yaml.tmp
kubectl get pvc

kubectl apply -f jenkins/k8s/jenkins-rbac.yaml.tmp

# Deploy lighter version of jenkins for testing
kubectl apply -f jenkins/k8s/jenkins-deployment.yaml.tmp

JENKINS_POD=$(kubectl get pod -n jenkins -l app=jenkins -o jsonpath="{.items[0].metadata.name}")
echo "$JENKINS_POD created"

kubectl apply -f jenkins/k8s/jenkins-service.yaml.tmp
kubectl apply -f jenkins/k8s/jenkins-ingress.yaml.tmp

# Get Public Ip address
kubectl get ingress jenkins-ingress -n jenkins

kubectl -n jenkins exec -it $JENKINS_POD cat /var/jenkins_home/secrets/initialAdminPassword

#cleanups
for file in $DIRECTORY/*.tmp; do
	rm $file
done