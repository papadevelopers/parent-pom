source "configs/dev.env"

echo "######## Starting VPC deployment with properties file : params.json"
source "./vpc/setup.sh"
echo "######## VPC CREATED #########"

echo "######## CREATING EKS #########"
source "./eks/setup.sh"
echo "######## EKS CREATED #########"

 eksctl utils associate-iam-oidc-provider \
   --cluster=$ENVIRONMENT \
   --region $REGION \
   --name=dev \
   --approve
   
kubectl delete ingressClass nginx -n kube-system
echo "######## CREATING EKS #########"
source "./eks/alb-install.sh"
echo "######## EKS CREATED #########"


VPC_NAME=eksctl-$ENVIRONMENT-cluster/VPC
echo "Deploying Reactive Microservices on aws cloud..."
echo "######## NOTE: Please copy EnvironmentName <VPC name> in Jenkinsfile for deployment. or Create a environment variable" 

echo "#### Fetching subnet list for future use #####"
# Use the AWS CLI to fetch the VPC ID for the given VPC name
VPC_ID=$(aws ec2 describe-vpcs --region "$REGION" --filters "Name=tag:Name,Values=$VPC_NAME" --query "Vpcs[].VpcId" --output text)
if [ -z "$VPC_ID" ]; then
    echo "No VPC found with name $VPC_NAME"
    exit 1
fi

# Use the AWS CLI to fetch the subnet list for the given VPC ID
echo "VPC ID: $VPC_ID"
PR_SUB_RESULT=$(aws ec2 describe-subnets --region "$REGION" --filters "Name=vpc-id,Values=$VPC_ID" "Name=tag:Name,Values=*Private*" --query "Subnets[].SubnetId" --output text | sed 's/\t/,/g')
IFS=',' read -ra PRIVATE_SUBNETS <<< "$PR_SUB_RESULT"
echo "PRIVATE SUBNETS ${PRIVATE_SUBNETS[0]},${PRIVATE_SUBNETS[1]}"
PU_SUB_RESULT=$(aws ec2 describe-subnets --region "$REGION" --filters "Name=vpc-id,Values=$VPC_ID" "Name=tag:Name,Values=*Public*" --query "Subnets[].SubnetId" --output text | sed 's/\t/,/g')
IFS=',' read -ra PUBLIC_SUBNETS <<< "$PU_SUB_RESULT"
echo "PUBLIC SUBNETS ${PUBLIC_SUBNETS[0]},${PUBLIC_SUBNETS[1]}"

echo "######## CREATING NAMESPACE $ENVIRONMENT #########"
aws eks --region us-east-1 update-kubeconfig --name "$ENVIRONMENT"
echo "######## $ENVIRONMENT NAMEPSCE CREATED #########"

echo "########## Create regcred for microservices ecr repos########"
echo "Linking regcred for docker"
kubectl create secret docker-registry regcred --docker-server="$ACC_ID".dkr.ecr.us-east-1.amazonaws.com --docker-username=AWS --docker-password="$ACC_ID".dkr.ecr.us-east-1.amazonaws.com
echo "########## Created ################"

echo "######## CREATING JENKINS #########"
source "./jenkins/setup.sh"
echo "######## JENKINS CREATED #########"

echo "######## CREATING INFRA #########"
kubectl create ns backend
source "./infra/keycloak/setup.sh"
echo "######## JENKINS INFRA #########"